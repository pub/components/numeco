# Diminuer l'impact environnemental d'un projet Gitlab

Ce dépôt fournit des components dont l'objectif est de diminuer l'impact environnemental des projets.

## Gitlab Project Doctor

Ce component exécute [Gitlab Project Doctor](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/gitlab-project-doctor) afin de nettoyer et réduire l'empreinte Gitlab d'un projet, en supprimant des informations obsolètes, telles que les anciens pipelines et artefacts ou les packages dupliqués.

> ⚠️ Veillez à bien renseigner un token (`GL_TOKEN` par exemple) dans les variables de CI (Settings / CI/CD / Variables) ayant les droits suffisants pour nettoyer le dépôt. Pour supprimer des pipelines, artifacts ou packages, un droit de Maintainer peut être nécessaire. Si l'input `token` n'est pas renseigné, le programme prend le token de la variable d'environnement `GL_TOKEN` si elle est rensignée ou à défaut le token de job `CI_JOB_TOKEN` qui n'a que très peu de droits.⚠️

```yaml
include:
  - component: gitlab-forge.din.developpement-durable.gouv.fr/pub/components/numeco/gitlab-project-doctor@<VERSION>
    inputs:
      stage: .pre
      token: $MY_GL_TOKEN
```

| Input | Valeur par défaut | Description |
| ----- | ------------- | ----------- |
| `stage`  | .pre | Le stage sur lequel est exécuté le nettoyage |
| `analysis_mode` | false | Mode analyse : aucune action destructive n'est exécutée |
| `days`     | 30 | Nombre de jours d'ancienneté à partir duquel un élément est considéré obsolète |
| `token` |  | Token d'accès pour le nettoyage à place de `GL_TOKEN` |
| `url` | `$CI_PROJECT_URL` | URL du projet à nettoyer, par défaut le projet courant |


## Contributions

Les contributions sont bienvenues. Veuillez lire [la documentation](https://docs.gitlab.com/ee/ci/components) sur la création de components CI/CD.
